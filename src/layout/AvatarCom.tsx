import React from 'react'
import { Avatar, Badge, Menu, Dropdown, message } from 'antd'
import { UserOutlined } from '@ant-design/icons'
import IconFont from '@component/IconFont'
import './AvatrCpm.modules.css'
interface MyProps {
  count: number // 必要属性
  src: string
}
const AvatarCom = (props: MyProps) => {
  const onClick = ({ key }: { key: any }) => {
    message.info(`Click on item ${key}`)
  }
  const menu = (
    <Menu onClick={onClick}>
      <Menu.Item key="1">
        <IconFont type="icon-sanmingzhi" />
        1st menu item
      </Menu.Item>
      <Menu.Item key="2">
        <IconFont type="icon-sanmingzhi" />
        2nd menu item
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item key="3">
        <IconFont type="icon-sanmingzhi" />
        退出登录
      </Menu.Item>
    </Menu>
  )
  return (
    <div className="avatar-item">
      <Badge count={props.count}>
        <Dropdown className="dropdown" overlay={menu} placement="bottomRight" arrow={true}>
          <Avatar src={props.src} shape="square" icon={<UserOutlined />} />
        </Dropdown>
      </Badge>
    </div>
  )
}

export default AvatarCom
