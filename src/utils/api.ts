import http from './axios'
interface params {
  data?: object
}
// 获取个人列表
export function getUserInfo(data?: params) {
  return http.get('/user', data, true)     //true动态开启loading值
}