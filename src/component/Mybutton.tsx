import React from 'react';
import {Button} from 'antd';
// 2.使用interface完全声明定义属性类型
interface MyProps {
  type: any; // 必要属性
  msg?: string; //可选属性
  children?: string; //可选属性
}
const Mybutton = (props: MyProps) => {
  return (
    <div>
      <Button type={props.type}>{props.msg}</Button>
    </div>
  );
};

export default Mybutton;
