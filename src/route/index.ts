
import loadable from '@loadable/component'
const routes = [
  {
    path: "/login",
    name: '登陆',
    component: loadable(() => import('@view/login/login')),
    icon: 'icon-pisa',
  },
  {
    path: "/home",
    name: 'home',
    icon: 'icon-qiyiguo',
    children: [
      {
        path: "/home/table",
        name: 'table',
        component: loadable(() => import('@view/home/Table')),
        icon: 'icon-sanmingzhi',
      },
      {
        path: "/home/glass",
        name: 'glass',
        component: loadable(() => import('@view/home/Glass')),
        icon: 'icon-shengnvguo',
      }
    ]
  },
  {
    path: "/about",
    name: 'about',
    component: loadable(() => import('@view/about/about')),
    icon: 'icon-tusi'
  },
  {
    path: "/list",
    name: 'list',
    component: loadable(() => import('@view/list/list')),
    icon: 'icon-tusi'
  },

];

export default routes
