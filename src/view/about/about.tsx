// About/index.jsx
import React, { useState } from 'react'
import SearchHeader from '@component/SearchHeader'
import { Popconfirm, Table } from 'antd'

export default function About() {
  const [selectedRowKeys, setSelectedRowKeys] = useState([])
  const onSelectChange = (selectedRowKeys: []) => {
    console.log('selectedRowKeys changed: ', selectedRowKeys)
    setSelectedRowKeys(selectedRowKeys)
  }
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name'
    },
    {
      title: 'Age',
      dataIndex: 'age'
    },
    {
      title: 'Address',
      dataIndex: 'address'
    },
    {
      title: 'operation',
      dataIndex: 'operation',
      render: (_: any, record: { key: React.Key }) =>
        setSelectedRowKeys.length >= 1 ? (
          <Popconfirm title="Sure to delete?" onConfirm={() => handleDelete(record.key)}>
            <a>Delete</a>
          </Popconfirm>
        ) : null
    }
  ]
  const [count, setCount] = useState(0)
  const [data, setData] = useState<any>([])
  for (let i = 0; i < 46; i++) {
    data.push({
      key: i,
      name: `Edward King ${i}`,
      age: 32,
      address: `London, Park Lane no. ${i}`
    })
  }
  interface DataType {
    key: React.Key
    name: string
    age: string
    address: string
  }
  const handleDelete = (key: React.Key) => {
    const dataSource = [...data]
    setData(dataSource.filter((item) => item.key !== key))
  }
  const rowSelection: any = {
    selectedRowKeys,
    onChange: onSelectChange,
    selections: [
      Table.SELECTION_ALL,
      Table.SELECTION_INVERT,
      Table.SELECTION_NONE,
      {
        key: 'odd',
        text: 'Select Odd Row',
        onSelect: (changableRowKeys: []) => {
          let newSelectedRowKeys = []
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return false
            }
            return true
          })
          setSelectedRowKeys(newSelectedRowKeys)
        }
      },
      {
        key: 'even',
        text: 'Select Even Row',
        onSelect: (changableRowKeys: []) => {
          let newSelectedRowKeys = []
          newSelectedRowKeys = changableRowKeys.filter((key, index) => {
            if (index % 2 !== 0) {
              return true
            }
            return false
          })
          setSelectedRowKeys(newSelectedRowKeys)
        }
      }
    ]
  }
  return (
    <div>
      <SearchHeader />
      <Table rowSelection={rowSelection} columns={columns} dataSource={data} />;
    </div>
  )
}
