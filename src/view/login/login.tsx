import React, {useState, useEffect} from 'react';
import {getUserInfo} from '@utils/api';

const login = () => {
  let [user, setUser] = useState({name: '', age: 0});
  useEffect(() => {
    (async function () {
      const userResult = await getUserInfo({});
      setUser(userResult.data.data);
    })();
    console.log(user);
  }, []);
  const fetchUser = async () => {
    const userResult = await getUserInfo({});
    console.log(userResult);
  };
  return (
    <div>
      {' '}
      user: {user?.name} <button onClick={fetchUser}>点我请求</button>{' '}
    </div>
  );
};

export default login;
